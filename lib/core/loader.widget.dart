import 'package:doggies_ceo/core/progress-indicator.widget.dart';
import 'package:flutter/material.dart';

class Loader extends StatelessWidget {
  final object;
  final Function callback;

  Loader({@required this.object, @required this.callback});

  @override
  Widget build(BuildContext context) {
    if (object == null) {
      return Center(
        child: GenericProgressIndicator(),
      );
    }

    if (object.length == 0) {
      return Center(
        child: Text(
          'Não há sub-raças para essa raça.',
          style: TextStyle(
            color: Theme.of(context).primaryColor.withOpacity(0.75),
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    }

    return callback();
  }
}
