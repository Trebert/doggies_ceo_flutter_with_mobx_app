import 'package:doggies_ceo/core/progress-indicator.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LoaderProgressIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: GenericProgressIndicator(),
    );
  }
}
