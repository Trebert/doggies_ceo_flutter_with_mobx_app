import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:doggies_ceo/features/bloc/breed.bloc.dart';
import 'package:doggies_ceo/features/bloc/dog.bloc.dart';
import 'package:doggies_ceo/features/bloc/dogsByBreed.bloc.dart';
import 'package:doggies_ceo/features/ui/shared/pages/home.page.dart';
import 'package:doggies_ceo/features/ui/themes/themes.widget.dart';
import 'package:doggies_ceo/features/utils/settings.dart';
import 'package:flutter/material.dart';

void main() => runApp(DoggiesApp());

class DoggiesApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: [
        Bloc((i) => BreedBloc()),
        Bloc((i) => DogBloc()),
        Bloc((i) => DogsByBreedBloc()),
      ],
      child: MaterialApp(
        title: 'Doggies.Ceo',
        debugShowCheckedModeBanner: false,
        theme: Themes().getSelectedTheme(),
        home: Container(
          color: Settings.getUnsafeColorBackground(),
          child: SafeArea(
            child: HomePage(),
          ),
        ),
      ),
    );
  }
}
