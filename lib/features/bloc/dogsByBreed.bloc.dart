import 'dart:async';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:doggies_ceo/features/models/dog.model.dart';
import 'package:doggies_ceo/features/repositories/dog.repository.dart';

class DogsByBreedBloc extends BlocBase {
  final DogRepository _repository = DogRepository();
  final StreamController<List<Dog>> _dogsController =
      StreamController<List<Dog>>.broadcast();
  Stream get outDogs => _dogsController.stream;
  final int quantity = 10;
  String _selectedValue = '';
  String _breedName = '';
  List<Dog> dogs = [];

  DogsByBreedBloc() {
    _dogsController.sink.add(dogs);
  }

  setNewSelectedDropDownValue(String subBreedName) {
    _selectedValue = subBreedName;
    notifyListeners();
    _getDogsByBreedAndSubBreed();
  }

  setBreedName(String breedName) {
    _breedName = breedName;
  }

  getSelectedDropDownValue() {
    return _selectedValue;
  }

  _getDogsByBreedAndSubBreed() async {
    if (_selectedValue != '') {
      dogs = await _repository.getRandomDogsByBreedAndSubBreed(
          _breedName, _selectedValue, quantity);
    } else {
      dogs = await _repository.getRandomDogsByBreed(_breedName, quantity);
    }
    _dogsController.sink.add(dogs);
  }

  @override
  void dispose() {
    _dogsController.close();
    super.dispose();
  }
}
