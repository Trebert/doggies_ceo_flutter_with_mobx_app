import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:doggies_ceo/features/models/dog.model.dart';
import 'package:doggies_ceo/features/repositories/dog.repository.dart';

class DogBloc extends BlocBase {
  final DogRepository _repository = DogRepository();
  Map<String, Dog> dogs = Map<String, Dog>();

  DogBloc();

  getDogByBreed(String breedName) async {
    if (dogs[breedName] == null) {
      dogs[breedName] = await _repository.getRandomDogByBreed(breedName);
    }
    notifyListeners();
  }
}
