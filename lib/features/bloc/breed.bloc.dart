import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:doggies_ceo/features/models/breed.model.dart';
import 'package:doggies_ceo/features/repositories/breed.repository.dart';

class BreedBloc extends BlocBase {
  final BreedRepository _repository = BreedRepository();
  List<Breed> breedsList;
  BreedBloc();

  StreamController<List<Breed>> _breedStreamController =
      StreamController<List<Breed>>();
  Stream get outBreeds => _breedStreamController.stream;

  getAllBreeds() async {
    breedsList = await _repository.getAllBreeds();
    _breedStreamController.sink.add(breedsList);
  }

  @override
  void dispose() {
    _breedStreamController.close();
    super.dispose();
  }
}
