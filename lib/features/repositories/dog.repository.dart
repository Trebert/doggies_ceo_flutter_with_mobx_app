import 'package:dio/dio.dart';
import 'package:doggies_ceo/features/models/dog.model.dart';
import 'package:doggies_ceo/features/utils/settings.dart';

class DogRepository {
  //Private methods
  Future<List<Dog>> _getAllDogsBy(String url) async {
    Response res = await Dio().get(url);

    return (res.data['message'] as List)
        .map((dog) => Dog.fromJson(dog))
        .toList();
  }

  Future<Dog> _getRandomDogBy(String url) async {
    Response res = await Dio().get(url);

    return Dog.fromJson(res.data['message']);
  }

  //Public Methods
  Future<Dog> getRandomDog() async {
    String url = '${Settings.apiUrl}breeds/image/random';

    Response res = await Dio().get(url);

    return Dog.fromJson(res.data['message']);
  }

  Future<List<Dog>> getAllDogsByBreed(String breedName) async {
    var url = '${Settings.apiUrl}breed/$breedName/images';

    return _getAllDogsBy(url);
  }

  Future<List<Dog>> getAllDogsBySubBreed(
      String breedName, String subBreedName) async {
    var url = '${Settings.apiUrl}breed/$breedName/$subBreedName/images';

    return _getAllDogsBy(url);
  }

  Future<Dog> getRandomDogByBreed(String breedName) async {
    var url = '${Settings.apiUrl}breed/$breedName/images/random';

    return _getRandomDogBy(url);
  }

  Future<Dog> getRandomDogByBreedAndSubBreed(
      String breedName, String subBreedName) async {
    var url = '${Settings.apiUrl}breed/$breedName/$subBreedName/images/random';

    return _getRandomDogBy(url);
  }

  Future<List<Dog>> getRandomDogsByBreed(String breedName, int quantity) async {
    var url = '${Settings.apiUrl}breed/$breedName/images/random/$quantity';

    return _getAllDogsBy(url);
  }

  Future<List<Dog>> getRandomDogsByBreedAndSubBreed(
      String breedName, String subBreedName, int quantity) async {
    var url =
        '${Settings.apiUrl}breed/$breedName/$subBreedName/images/random/$quantity';

    return _getAllDogsBy(url);
  }
}
