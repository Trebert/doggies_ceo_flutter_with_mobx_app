import 'package:dio/dio.dart';
import 'package:doggies_ceo/features/models/breed.model.dart';
import 'package:doggies_ceo/features/utils/settings.dart';

class BreedRepository {
  Future<List<Breed>> getAllBreeds() async {
    var url = '${Settings.apiUrl}breeds/list/all';
    List<Breed> breeds = List<Breed>();

    Response res = await Dio().get(url);

    res.data['message'].forEach((key, value) {
      breeds.add(Breed.fromJson(key, value));
    });
    return breeds;
  }
}
