import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:doggies_ceo/features/bloc/dogsByBreed.bloc.dart';
import 'package:doggies_ceo/features/models/breed.model.dart';
import 'package:doggies_ceo/features/utils/settings.dart';
import 'package:flutter/material.dart';

class SubBreedCard extends StatelessWidget {
  final Breed subBreed;
  final double totalSize;

  SubBreedCard({@required this.totalSize, @required this.subBreed});

  @override
  Widget build(BuildContext context) {
    return Consumer<DogsByBreedBloc>(
      builder: (ctx, bloc) {
        return Container(
          height: totalSize * 0.80,
          width: totalSize * 0.80,
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(
            color: subBreed.name == bloc.getSelectedDropDownValue().toString()
                ? Theme.of(context).primaryColor.withOpacity(0.3)
                : Theme.of(context).primaryColor,
            borderRadius: BorderRadius.all(
              Radius.circular(totalSize * 0.80),
            ),
          ),
          child: FlatButton(
            child: Text(
              subBreed.name,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: Settings.getRelativeFontSize(context, 9),
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
            ),
            onPressed: () {
              bloc.setNewSelectedDropDownValue(subBreed.name);
            },
          ),
        );
      },
    );
  }
}
