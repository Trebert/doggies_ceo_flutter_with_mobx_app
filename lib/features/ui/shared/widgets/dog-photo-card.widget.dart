import 'package:doggies_ceo/features/models/dog.model.dart';
import 'package:flutter/material.dart';

class DogPhotoCard extends StatelessWidget {
  final Dog dog;

  DogPhotoCard({@required this.dog});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Image.network(
            dog.image ?? '',
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}
