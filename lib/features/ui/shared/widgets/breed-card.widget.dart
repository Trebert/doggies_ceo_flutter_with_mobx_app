import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:doggies_ceo/features/bloc/dog.bloc.dart';
import 'package:doggies_ceo/features/models/breed.model.dart';
import 'package:doggies_ceo/features/ui/shared/pages/dogs-by-breed.page.dart';
import 'package:flutter/material.dart';

class BreedCard extends StatelessWidget {
  final Breed breed;
  BreedCard({@required this.breed});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (ctx) => DogsByBreed(
              breed.name,
            ),
          ),
        );
      },
      child: Card(
        elevation: 7.0,
        margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Consumer<DogBloc>(
            builder: (BuildContext ctx, DogBloc bloc) {
              bloc.getDogByBreed(breed.name);
              if (bloc.dogs[breed.name] != null) {
                return _getTile(
                  bloc.dogs[breed.name].breedName,
                  bloc.dogs[breed.name].image,
                  bloc.dogs[breed.name].subBreedName,
                );
              } else {
                return _getTile(breed.name, '', '');
              }
            },
          ),
        ),
      ),
    );
  }

  _getTile(breedName, image, subBreedName) {
    return ListTile(
      leading: CircleAvatar(
        radius: 30,
        backgroundColor: Colors.white,
        backgroundImage: NetworkImage(image ?? " "),
      ),
      title: Text(breedName ?? ''),
      subtitle: Text(subBreedName ?? ''),
      trailing: Icon(Icons.keyboard_arrow_right),
    );
  }
}
