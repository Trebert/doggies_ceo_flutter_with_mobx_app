import 'package:doggies_ceo/core/loader.widget.dart';
import 'package:doggies_ceo/features/models/breed.model.dart';
import 'package:doggies_ceo/features/ui/shared/widgets/sub-breed-card.widget.dart';
import 'package:doggies_ceo/features/utils/settings.dart';
import 'package:flutter/material.dart';

class SubBreedList extends StatelessWidget {
  final List<Breed> subBreeds;
  final double totalHeight;

  SubBreedList({@required this.totalHeight, @required this.subBreeds});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: totalHeight,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: totalHeight * 0.2,
            child: FittedBox(
              child: Text(
                'Sub-Raças',
                style: TextStyle(
                  fontSize: Settings.getRelativeFontSize(context, 18),
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
          ),
          Container(
            height: (totalHeight * 0.8),
            child: Loader(
              object: subBreeds,
              callback: list,
            ),
          ),
        ],
      ),
    );
  }

  list() {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: subBreeds.length,
      itemBuilder: (ctx, idx) {
        return Padding(
          padding: EdgeInsets.all(5),
          child: SubBreedCard(
            totalSize: (totalHeight * 0.8),
            subBreed: subBreeds[idx],
          ),
        );
      },
    );
  }
}
