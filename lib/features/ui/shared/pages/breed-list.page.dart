import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:doggies_ceo/core/loader-progress-indicator.widget.dart';
import 'package:doggies_ceo/features/bloc/breed.bloc.dart';
import 'package:doggies_ceo/features/ui/shared/widgets/breed-card.widget.dart';
import 'package:flutter/material.dart';

class BreedList extends StatelessWidget {
  final BreedBloc bloc = BlocProvider.getBloc<BreedBloc>();

  @override
  Widget build(BuildContext context) {
    bloc.getAllBreeds();
    return StreamBuilder(
      stream: bloc.outBreeds,
      builder: (ctx, snp) {
        if (snp.hasData) {
          return ListView.builder(
            itemCount: snp.data.length,
            itemBuilder: (ctx, idx) {
              return BreedCard(breed: snp.data[idx]);
            },
          );
        } else {
          return LoaderProgressIndicator();
        }
      },
    );
  }
}
