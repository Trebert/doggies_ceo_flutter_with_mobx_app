import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:doggies_ceo/core/progress-indicator.widget.dart';
import 'package:doggies_ceo/features/bloc/breed.bloc.dart';
import 'package:doggies_ceo/features/bloc/dogsByBreed.bloc.dart';
import 'package:doggies_ceo/features/models/breed.model.dart';
import 'package:doggies_ceo/features/ui/shared/widgets/dog-photo-card.widget.dart';
import 'package:doggies_ceo/features/ui/shared/widgets/sub-breed-list.widget.dart';
import 'package:doggies_ceo/features/utils/settings.dart';
import 'package:flutter/material.dart';

class DogsByBreed extends StatelessWidget {
  DogsByBreedBloc dogsByBreedBloc = BlocProvider.getBloc<DogsByBreedBloc>();
  Breed _breed;

  DogsByBreed(String breedName) {
    BreedBloc breedBloc = BlocProvider.getBloc<BreedBloc>();
    _breed = breedBloc.breedsList.where((b) => b.name == breedName).first;
    dogsByBreedBloc.setBreedName(breedName);
    dogsByBreedBloc.setNewSelectedDropDownValue(
        _breed.subBreeds.length > 0 ? _breed.subBreeds[0].name : '');
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      title: Text(
        _breed.name,
        style: Theme.of(context).textTheme.title,
      ),
    );

    return Scaffold(
      appBar: appBar,
      body: Container(
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: Settings.getSafeRelativeHeight(context, 0.05, appBar),
              ),
              SubBreedList(
                totalHeight:
                    Settings.getSafeRelativeHeight(context, 0.2, appBar),
                subBreeds: _breed.subBreeds,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                child: Container(
                  height: Settings.getSafeRelativeHeight(context, 0.75, appBar),
                  width: double.infinity,
                  child: StreamBuilder(
                    stream: dogsByBreedBloc.outDogs,
                    builder: (ctx, snap) {
                      if (snap.hasError) {
                        return Text(
                          'Erro ao carregar dados.',
                          style: TextStyle(
                            color: Theme.of(context)
                                .primaryColor
                                .withOpacity(0.75),
                            fontSize: Settings.getRelativeFontSize(context, 16),
                            fontWeight: FontWeight.bold,
                          ),
                        );
                      }
                      if (snap.hasData) {
                        return ListView.builder(
                          itemCount: snap.data.length,
                          itemBuilder: (ctx, idx) {
                            return DogPhotoCard(
                              dog: snap.data[idx],
                            );
                          },
                        );
                      } else {
                        return GenericProgressIndicator();
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
