import 'package:doggies_ceo/features/ui/shared/pages/breed-list.page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Doggies',
          style: Theme.of(context).textTheme.title,
        ),
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: BreedList(),
      ),
    );
  }
}
