import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:doggies_ceo/features/bloc/dogsByBreed.bloc.dart';
import 'package:doggies_ceo/features/models/breed.model.dart';
import 'package:flutter/material.dart';

class AndroidDropDownButton extends StatelessWidget {
  final String hintText;
  final Breed breed;

  AndroidDropDownButton({@required this.breed, @required this.hintText});

  @override
  Widget build(BuildContext context) {
    return Consumer<DogsByBreedBloc>(builder: (ctx, bloc) {
      return DropdownButton<String>(
        items: breed.subBreeds.map(
          (subBreed) {
            return DropdownMenuItem(
              value: subBreed.name,
              child: Text(subBreed.name),
            );
          },
        ).toList(),
        hint: Text(hintText),
        onChanged: (newValue) {
          bloc.setNewSelectedDropDownValue(newValue);
        },
        value: bloc.getSelectedDropDownValue(),
      );
    });
  }
}
