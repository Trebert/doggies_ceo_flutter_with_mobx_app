import 'package:doggies_ceo/features/ui/themes/dark.theme.dart';
import 'package:doggies_ceo/features/ui/themes/light.theme.dart';
import 'package:doggies_ceo/features/utils/settings.dart';
import 'package:flutter/material.dart';

class Themes {
  ThemeData getSelectedTheme() {
    const String _lightTheme = 'light';
    // const String _darkTheme = 'light';

    return Settings.selectedTheme == _lightTheme ? lightTheme() : darkTheme();
  }
}
