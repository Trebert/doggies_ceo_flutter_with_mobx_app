import 'package:flutter/material.dart';

const brightness = Brightness.light;
const primaryColor = const Color(0xFF01579B);
const lightColor = const Color(0xFFB3E5FC);
const accentColor = const Color(0xFF002F6C);
const backgroundColor = const Color(0xFFF5F5F5);
const dangerColor = const Color(0xFFE6FFFF);

ThemeData darkTheme() {
  return ThemeData(
    brightness: brightness,
    primaryColor: primaryColor,
    primaryColorLight: lightColor,
    accentColor: accentColor,
    backgroundColor: backgroundColor,
    cardColor: Colors.white,
    textTheme: TextTheme(
      title: TextStyle(
        color: Colors.white,
      ),
      headline: TextStyle(
        color: accentColor,
        fontSize: 26,
        fontWeight: FontWeight.bold,
      ),
      subhead: TextStyle(
        color: primaryColor,
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
      display1: TextStyle(
        color: accentColor,
        fontSize: 16,
        fontWeight: FontWeight.w500,
      ),
      display2: TextStyle(
        color: primaryColor,
        fontSize: 16,
        fontWeight: FontWeight.w500,
      ),
      display3: TextStyle(
        color: Colors.white,
        fontSize: 16,
        fontWeight: FontWeight.w500,
      ),
      display4: TextStyle(
        color: dangerColor,
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
    ),
  );
}
