import 'package:doggies_ceo/features/utils/settings.dart';

class Dog {
  String name;
  String image;
  String breedName;
  String subBreedName;

  Dog.fromJson(String message) {
    this.image = message;
    message = message.replaceAll(Settings.apiUrlImageDefault, '');
    message = message.split('/')[0];

    var names = message.split('-');

    if (names.length > 0 && names.length < 2) {
      this.breedName = names[0];
    } else if (names.length >= 2) {
      this.breedName = names[0];
      this.subBreedName = names[1];
    }
  }
}
