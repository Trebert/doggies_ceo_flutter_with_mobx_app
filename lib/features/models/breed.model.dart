class Breed {
  String name;
  List<Breed> subBreeds;

  Breed.fromJson(String name, List subBreeds) {
    this.name = name;
    this.subBreeds = List<Breed>();
    for (String sub in subBreeds) {
      this.subBreeds.add(Breed.fromJson(sub, List<Breed>()));
    }
  }
}
