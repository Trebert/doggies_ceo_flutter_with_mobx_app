import 'package:flutter/material.dart';

class Settings {
  static String apiUrl = 'https://dog.ceo/api/';
  static String apiUrlImageDefault = 'https://images.dog.ceo/breeds/';
  static String selectedTheme = 'light';
  static final String _lightColorTheme = 'light';

  static Color getUnsafeColorBackground() {
    return selectedTheme == _lightColorTheme
        ? Color(0xFF002F6C)
        : Color(0xFF000000);
  }

  static double getTotalRelativeHeight(BuildContext ctx, double percentSize) {
    return MediaQuery.of(ctx).size.height * percentSize;
  }

  static double getTotalRelativeWidth(BuildContext ctx, double percentSize) {
    return MediaQuery.of(ctx).size.width * percentSize;
  }

  static double getSafeRelativeHeight(
      BuildContext ctx, double percentSize, AppBar appBar) {
    return (MediaQuery.of(ctx).size.height -
            (appBar.preferredSize.height + MediaQuery.of(ctx).padding.top)) *
        percentSize;
  }

  static getRelativeFontSize(BuildContext ctx, double relativeSize) {
    return relativeSize * MediaQuery.of(ctx).textScaleFactor;
  }
}
