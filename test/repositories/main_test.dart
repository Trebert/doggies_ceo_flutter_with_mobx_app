import 'package:doggies_ceo/features/models/breed.model.dart';
import 'package:doggies_ceo/features/models/dog.model.dart';
import 'package:doggies_ceo/features/repositories/breed.repository.dart';
import 'package:doggies_ceo/features/repositories/dog.repository.dart';
import 'package:flutter_test/flutter_test.dart';

main() async {
  var _breedRepository = BreedRepository();
  var _dogRepository = DogRepository();

  group('All', () {
    group('BreedRepository', () {
      group('AllBreeds', () {
        test('should_return_all_breeds', () async {
          var breeds = await _breedRepository.getAllBreeds();
          expect(breeds is List<Breed>, true);
          expect((breeds.length > 1), true);
        });
      });
    });

    group('DogRepository', () {
      group('GetRandomDog', () {
        test('should_return_a_random_dog', () async {
          var data = await _dogRepository.getRandomDog();

          expect(data is Dog, true);
        });
      });

      group('AllDogsByBreed', () {
        test('should_return_dogs_without_subBreed', () async {
          var breedName = 'beagle';

          var data = await _dogRepository.getAllDogsByBreed(breedName);

          expect((data.length > 0), true);
          expect(data is List<Dog>, true);
          expect(data[0].breedName == breedName, true);
        });

        test('should_return_dogs_with_breed', () async {
          var breedName = 'hound';

          var data = await _dogRepository.getAllDogsByBreed(breedName);

          expect((data.length > 0), true);
          expect(data is List<Dog>, true);
          expect(data[0].breedName == breedName, true);
          expect(data[0].subBreedName != null, true);
        });
      });

      group('AllDogsBySubBreed', () {
        test('should_return_all_dogs_by_subBreed', () async {
          String breedName = 'hound';
          String subBreedName = 'afghan';

          var data = await _dogRepository.getAllDogsBySubBreed(
              breedName, subBreedName);

          expect((data.length > 0), true);
          expect((data is List<Dog>), true);
          expect(data[0].breedName == breedName, true);
          expect(data[0].subBreedName == subBreedName, true);
        });
      });

      group('RandomDogByBreed', () {
        test('should_return_a_random_dog_by_breed', () async {
          var breedName = 'beagle';
          var data = await _dogRepository.getRandomDogByBreed(breedName);

          expect(data is Dog, true);
          expect(data.breedName == breedName, true);
        });
      });

      group('RandomDogByBreedAndSubBreed', () {
        test('should_return_a_dog_breed_and_subBreed', () async {
          var breedName = 'hound';
          String subBreedName = 'afghan';

          var data = await _dogRepository.getRandomDogByBreedAndSubBreed(
              breedName, subBreedName);

          expect(data is Dog, true);
          expect(data.breedName == breedName, true);
          expect(data.subBreedName == subBreedName, true);
        });
      });

      group('RandomDogsByBreed', () {
        test('should_return_random_dogs_by_breed_using_quantity', () async {
          String breedName = 'hound';
          int quantity = 10;

          var data =
              await _dogRepository.getRandomDogsByBreed(breedName, quantity);

          expect(data is List<Dog>, true);
          expect(data.length == quantity, true);
          expect(data[0].breedName == breedName, true);
        });
      });

      group('RandomDogsByBreedAndSubBreed', () {
        test('should_return_a_random_dog_by_breed_and_subBreed_using_quantity',
            () async {
          String breedName = 'hound';
          String subBreedName = 'afghan';
          int quantity = 10;

          var data = await _dogRepository.getRandomDogsByBreedAndSubBreed(
              breedName, subBreedName, quantity);

          expect(data is List<Dog>, true);
          expect(data.length == quantity, true);
          expect(data[0].breedName == breedName, true);
          expect(data[0].subBreedName == subBreedName, true);
        });
      });
    });
  });
}
